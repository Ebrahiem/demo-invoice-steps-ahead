# -*- coding: utf-8 -*-
{
    'name': "Demo invoice",
    'description': """
        invoice
    """,
    'author': "Ebrahiem",
    'category': 'accounting',
    'version': '0.1',
    'license': 'AGPL-3',
    'depends': ['base', 'account'],
    'data': [
        'views/partner.xml',
        'reports/invoice_inherit_report.xml',
    ],
}
