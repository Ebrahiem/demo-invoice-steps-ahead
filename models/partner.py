# -*- coding: utf-8 -*-
import qrcode
import base64
from io import BytesIO
from odoo import models, api, fields, _


class AccountMovefeesInherit(models.Model):
    _inherit = "account.move"

    custom_extra_fees = fields.Float('الشحن والجمارك والتوصيل')
    custom_service_fees = fields.Float('رسوم الخدمة')
    usd_rate_exchange = fields.Float(compute='get_rate_exchange')
    defualt_usd = fields.Boolean(compute='get_defualt_usd')
    amount_in_sar = fields.Float(compute='get_total_amount_sar', string='المجموع بالريال السعودي')

    def get_rate_exchange(self):
        rate = self.env['ir.config_parameter'].get_param('demo-invoice-steps-ahead.usd_rate') or 1.0
        self.usd_rate_exchange = rate

    def get_defualt_usd(self):
        defualt_usd = self.currency_id.id
        if defualt_usd == 2:
            self.defualt_usd = True
        else:
            self.defualt_usd = False

    def get_total_amount_sar(self):
        if self.defualt_usd:
            rate = self.env['ir.config_parameter'].get_param('demo-invoice-steps-ahead.usd_rate') or 1.0
            self.amount_in_sar = float(self.amount_residual) * float(rate)

        else:
            self.amount_in_sar = self.amount_residual

    def _compute_amount(self):
        for move in self:
            if move.payment_state == 'invoicing_legacy':
                move.payment_state = move.payment_state
                continue

            total_untaxed = 0.0
            total_untaxed_currency = 0.0
            total_tax = 0.0
            total_tax_currency = 0.0
            total_to_pay = 0.0
            total_residual = 0.0
            total_residual_currency = 0.0
            total = 0.0
            total_currency = 0.0
            total_fees_percentage = 0
            currencies = set()

            for line in move.line_ids:
                if line.currency_id and line in move._get_lines_onchange_currency():
                    currencies.add(line.currency_id)

                if move.is_invoice(include_receipts=True):
                    # === Invoices ===

                    if not line.exclude_from_invoice_tab:
                        # Untaxed amount.
                        total_untaxed += line.balance
                        total_untaxed_currency += line.amount_currency
                        total += line.balance
                        total_currency += line.amount_currency
                    elif line.tax_line_id:
                        # Tax amount.
                        total_tax += line.balance
                        total_tax_currency += line.amount_currency
                        total += line.balance
                        total_currency += line.amount_currency
                    elif line.account_id.user_type_id.type in ('receivable', 'payable'):
                        # Residual amount.
                        total_to_pay += line.balance
                        total_residual += line.amount_residual
                        total_residual_currency += line.amount_residual_currency
                else:
                    # === Miscellaneous journal entry ===
                    if line.debit:
                        total += line.balance
                        total_currency += line.amount_currency

            if move.move_type == 'entry' or move.is_outbound():
                sign = 1
            else:
                sign = -1
            move.amount_untaxed = sign * (total_untaxed_currency if len(currencies) == 1 else total_untaxed)
            move.amount_tax = sign * (total_tax_currency if len(currencies) == 1 else total_tax)
            tax_perce = (move.amount_tax / move.amount_untaxed) * 100 if move.amount_untaxed != 0 else 0

            total_fees_percentage = tax_perce * (move.custom_service_fees + move.custom_extra_fees)/100
            move.amount_total = sign * (
                total_currency - move.custom_service_fees - move.custom_extra_fees - total_fees_percentage if len(
                    currencies) == 1 else total - total_fees_percentage)
            move.amount_residual = -sign * (
                total_residual_currency + move.custom_service_fees + move.custom_extra_fees + total_fees_percentage if len(
                    currencies) == 1 else total_residual + move.custom_service_fees + move.custom_extra_fees + total_fees_percentage)
            move.amount_untaxed_signed = -total_untaxed - move.custom_service_fees - move.custom_extra_fees
            move.amount_tax_signed = -total_tax - move.custom_service_fees - move.custom_extra_fees - total_fees_percentage
            move.amount_total_signed = abs(
                total + move.custom_service_fees + move.custom_extra_fees - total_fees_percentage) if move.move_type == 'entry' else -total + move.custom_service_fees + move.custom_extra_fees + total_fees_percentage
            move.amount_residual_signed = total_residual +  move.custom_service_fees + move.custom_extra_fees + total_fees_percentage
            print(move.amount_residual_signed)

            currency = len(currencies) == 1 and currencies.pop() or move.company_id.currency_id

            # Compute 'payment_state'.
            new_pmt_state = 'not_paid' if move.move_type != 'entry' else False

            if move.is_invoice(include_receipts=True) and move.state == 'posted':

                if currency.is_zero(move.amount_residual):
                    reconciled_payments = move._get_reconciled_payments()
                    if not reconciled_payments or all(payment.is_matched for payment in reconciled_payments):
                        new_pmt_state = 'paid'
                    else:
                        new_pmt_state = move._get_invoice_in_payment_state()
                elif currency.compare_amounts(total_to_pay, total_residual) != 0:
                    new_pmt_state = 'partial'

            if new_pmt_state == 'paid' and move.move_type in ('in_invoice', 'out_invoice', 'entry'):
                reverse_type = move.move_type == 'in_invoice' and 'in_refund' or move.move_type == 'out_invoice' and 'out_refund' or 'entry'
                reverse_moves = self.env['account.move'].search(
                    [('reversed_entry_id', '=', move.id), ('state', '=', 'posted'), ('move_type', '=', reverse_type)])

                # We only set 'reversed' state in cas of 1 to 1 full reconciliation with a reverse entry; otherwise, we use the regular 'paid' state
                reverse_moves_full_recs = reverse_moves.mapped('line_ids.full_reconcile_id')
                if reverse_moves_full_recs.mapped('reconciled_line_ids.move_id').filtered(lambda x: x not in (
                        reverse_moves + reverse_moves_full_recs.mapped('exchange_move_id'))) == move:
                    new_pmt_state = 'reversed'

            move.payment_state = new_pmt_state


class configurationInherit(models.TransientModel):
    _inherit = "res.config.settings"

    usd_rate = fields.Float('USD Rate', config_parameter='demo-invoice-steps-ahead.usd_rate')


